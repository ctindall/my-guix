(use-modules (guix)
	     (guix packages)
	     (guix records)
	     (guix build-system gnu)
	     (guix build-system copy)			  
	     (guix build utils)
	     ((guix licenses) #:prefix licenses:)
	     (guix git-download)

	     (gnu)
	     (gnu packages autotools)
	     (gnu packages tls)
	     (gnu services web)
	     (gnu packages ncurses)
	     (gnu packages certs)
	     (gnu packages tls)
	     (gnu packages virtualization)
	     (gnu packages linux)
	     (gnu packages emacs)
	     (gnu packages screen)
	     (gnu packages dns)
	     (gnu packages vpn)
	     (gnu packages rsync)
	     (gnu packages version-control)
	     (gnu packages docker)

	     (gnu services web)
	     (gnu services certbot)
	     (gnu services networking)
	     (gnu services ssh)
	     (gnu services docker)
	     (gnu services dbus)
	     (gnu services desktop)
	     (gnu services shepherd)
	     (gnu services virtualization))

(define nsd2046-pwsafe
  (package
   (name "nsd20464-pwsafe")
   (version "2020-09-13")
   (source
    (origin
     (method git-fetch)
     (uri (git-reference
	   (url "https://github.com/nsd20463/pwsafe.git")
	   (commit "c49a0541b66647ad04d19ddb351d264054c67759")))
     (sha256
      (base32
       "0ak09r1l7k57m6pdx468hhzvz0szmaq42vyr575fvsjc8rbrp8qq"))))

   (build-system gnu-build-system)

   (native-inputs `(("autoconf" ,autoconf-2.68)
                    ("automake" ,automake)))

   (inputs `(("openssl" ,openssl)))

   (arguments
    '(#:configure-flags '("--disable-depenency-tracking")
			#:phases (modify-phases %standard-phases
						(add-before 'configure 'do-autoconf
							    (lambda* (#:key outputs #:allow-other-keys)
								     (invoke "aclocal")
								     (invoke "autoheader")
								     (invoke "automake" "--add-missing")
								     (invoke "autoconf")))
						(delete 'check)
						(add-before 'do-autoconf 'debug-shit
							    (lambda* (#:key outpus #:allow-other-keys)
								     (invoke "ls"))))))
   
   (synopsis "commandline tool compatible with Counterpane's Passwordsafe")
   (description "Development can be found at https://github.com/nsd20463/pwsafe

Releases and screenshots used to be found at http://nsd.dyndns.org/pwsafe
but that domain is dead due to things beyond my control. So there's just
github now.")
   (home-page "https://github.com/nsd20463/pwsafe")
   (license licenses:gpl2+)))

(define cams-desktop-rsa-key
   (plain-file "cams_desktop_rsa.pub" "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDNXg/AIxp7Qi46EBGeZ5mChdCyhKKMcsmNkOL6S2QR0oMcLUyv9OEZVe3YWUQpkt85E0TxktETnyzE/DzPDQogzNQvbXk6UJ7w5+sUKHC/fYFKaGU5gKP2eG6xv0/IL212lU20m3ElW5tEqvg3ZVyy1cgixJkA9cxLJsSyuPOC3+0eCM3hRY+XyRDFMcWQjABAe0S6MkreesFJXbJQxKPCVEYOamDvnavs9JYvemHBphmcd/W6BgEs9YTB9qQhtUd2fd/skvCvunF1zeyu5nlTZXqnaZFBQxtSlYZDYIB9gRmoiuSa1lHy6fHdONci9Qh8yM2nniT+57PE7GRkLw5RA5p12Y2VH4N2HcLcdlUFv3HrhjyNTab2wuvKqFBf0HLIMmuVjNWbgHRPQLi79TTxx1UrtYfR1nu0LHclvt15Tt+NruF61tUD+jNjp8A3+qAYA7z/+JQyoub16u5nYrmnmyyMTa1hOGrNcC7kIXH9vEi1VSbsjA81ssXIUh9IW8M= root@DESKTOP-GL6EKCV
"))

(define cam-at-tindall-dot-space-rsa-key
  (plain-file "cam-at-tindall-dot-space-rsa.pub" "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQC2wwrp0T1BSmaqYMutkdUIUjJy5dfyqf1vwxQuHNZLYN7tJXHcKcMCcN1eYVNF1F/HPgmAcixbPULpniHerxeejLGAB/4dRo4X7H8UJUwf0Hh9rsLMC9lrS3HedNkAMjeQo4/rzaNB0z16/THX1vJWHlu6vx4hCqiiZk94B87UWSeMmvETSZDhCWBv1Ga61HE/HclbzX16NwVC1k3HYIzvogRMVf+vb/Tk8//g+lpaC/UTHSbPDYQoYPMEsb70i11Spp2+P4PE275y5Gc1FtdM2jZIkXgFEQ1z/tWlwteYhXdhj/TQ3PSWAppG+JctCAmbg15b5BfCzzrPvagVGQ2n cam@tindall.space
"))

(define gitlab-deploy-key
  (plain-file "tindalldotspace_gitlab_rsa_key.pub" "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDCUfD8rzXdW2wFD+b3MdAiW24i1LRGVfQWtkFAuLpVLWdTu8rfhzz/ITzHJr2VdmZrcgC2k/c/tKoCSvwGVgyrxfiHdLBuUzbq/DgDDzuTuOVTMPgkST+xz6agtQUQ8N3hyTFkTdYl+KTtW7syoYAys8bJUg+6VU1WPK8NJ4zbgZ+Gep10jFmOWt2rk7bavy01CMnMYjLSFE0F5ZxlJYAhjneIddhu1fm/gzIeOAgddvq1cZsWrdedB0DMh4mnTe99edqw2jZK1zK0I+zpJ1IBgxCpBvhqOkaBOKUe876X+ums0RP4cVspShUmMliJtcTlHrYMD1DUhda60UXgCov3TnnsB4i/WCygz6iUEs23L+8n5MHpPzdd9ooOtGVHnN5eMWGd6lUsOB8jojpQkhKsS4ApW4CpNplTFoBYLKdT2rGDUJ9kG3dYOL407SfEyFr7UU13GXq0gQIobxUTgJWLm37numbLOL3CAEvTGZTv4ayC025qdxiDbWQht8qyAFs= cam@tindall.space"))

;;caddy
(define caddy
  (package
    (name "caddy")
    
    (version "2.4.0")
    
    (source
     (origin
       (method url-fetch/tarbomb)
       (uri (string-append "https://github.com/caddyserver/caddy/releases/download/v" version "/caddy_" version "_linux_amd64.tar.gz"))
       (sha256
	(base32
	 "0qwnllnggy0axmyzsymw9d3l1lmsfz1ckb4pp26ba57cl8bpmbmv"))))

    (build-system copy-build-system)

    (arguments
     '(#:install-plan '(("caddy" "bin/caddy"))))
    
    (synopsis "This is a *BAD* Caddy package. It just pulls the already-built binary from Github, rather than building from source.")
    (description "See https://caddyserver.com/")
    (home-page "https://caddyserver.com/")
    (license licenses:asl2.0)))

(define-record-type* <caddy-configuration>
  caddy-configuration  make-caddy-configuration caddy-configuration?)

(define caddy-service-type
  (service-type
   (name 'caddy)
   (description "Caddy service type")
   (extensions
    (list (service-extension shepherd-root-service-type
			     (compose list
				      (lambda (config)
					(shepherd-service
					 (documentation "Run the caddy daemon (caddy).")
					 (provision '(caddy))
					 (requirement '(user-processes))
					 (start #~(make-forkexec-constructor '("caddy" "start"
									       "-config"  "/etc/Caddyfile"
									       "-pidfile" "/var/run/caddy.pid")
									     #:pid-file "/var/run/caddy.pid"))
					 (stop #~(make-kill-destructor))))))))))

;;wireguard
(define-record-type* <wg-quick-configuration>
  wg-quick-configuration  make-wg-quick-configuration wg-quick-configuration?)

(define wg-quick-service-type
  (service-type
   (name 'wg-quick)
   (description "wg-quick (wireguard) service type")
   (extensions
    (list (service-extension shepherd-root-service-type
			     (compose list
				      (lambda (config)
					(shepherd-service
					 (documentation "Start the wg0 wireguard interface")
					 (provision '(wg-quick))
					 (requirement '(user-processes))
					 (one-shot? #t)
					 (start #~(make-forkexec-constructor '("wg-quick" "up" "wg0")))
					 (stop #~(make-forkexec-constructor '("wg-quick" "down" "wg0")))))))))))



(operating-system
 (host-name "new.tindall.space")
 (timezone "UTC")
 (locale "en_US.utf8")

 (bootloader (bootloader-configuration
              (bootloader grub-bootloader)
              (target "/dev/vda")))
 
 (file-systems (append (list (file-system
			      (device "/dev/vda2")
			      (mount-point "/")
			      (type "ext4"))
			     (file-system
			      (device "/dev/disk/by-id/scsi-0DO_Volume_tindalldotspace-home")
			      (mount-point "/home")
			      (type "ext4"))
			     (file-system
			      (device "/dev/disk/by-id/scsi-0DO_Volume_tindalldotspace-var-lib-docker")
			      (mount-point "/var/lib/docker")
			      (type "ext4"))
			     )
                       %base-file-systems))
 
 (groups %base-groups)
 
 (users (append (list (user-account
		       (name "cam")
		       (group "users")
		       (supplementary-groups (list "wheel" "docker"))
		       (home-directory "/home/cam"))
		      (user-account
		       (name "docker")
		       (group "users")
		       (supplementary-groups (list "docker"))
		       (home-directory "/home/docker")))
		%base-user-accounts))

 (sudoers-file
  (plain-file "sudoers"
              (string-append (plain-file-content %sudoers-specification)
                             (format #f "~a ALL = NOPASSWD: ALL~%"
                                     "cam"))))
 
 (packages (append (list ncurses ;; for whatever reason, this is the package that provides "clear"
			 le-certs nss-certs certbot
			 emacs screen ;;emacs really only needed for the eterm-color termcap file
			 net-tools (list isc-bind "utils") ;; netstat, dig, etc.
;			 caddy
			 nsd2046-pwsafe
			 wireguard-tools
			 rsync
			 git
			 docker-compose) 
		   %base-packages))
 
 (services (append (list (service dhcp-client-service-type)
			 (service openssh-service-type
				  (openssh-configuration
				   (permit-root-login #t)
				   (password-authentication? #t)
                                   (port-number 22)
				   (authorized-keys
				    `(("root" ,cams-desktop-rsa-key)
				      ("cam"  ,cams-desktop-rsa-key)
				      ("cam"  ,cam-at-tindall-dot-space-rsa-key)
				      ("docker" ,gitlab-deploy-key)))))
			 (dbus-service)
                         (elogind-service)
                         (service docker-service-type)
			 (service qemu-binfmt-service-type
				  (qemu-binfmt-configuration
				   (platforms (lookup-qemu-platforms "arm" "aarch64"))))
			 ;(service caddy-service-type
			 ;	  (caddy-configuration))
			 (service wg-quick-service-type
				  (wg-quick-configuration)))
                   %base-services)))
