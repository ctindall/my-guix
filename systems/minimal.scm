(use-modules (guix)
	     (guix packages)
	     (guix records)
	     (guix build-system gnu)
	     (guix build-system copy)			  
	     (guix build utils)
	     ((guix licenses) #:prefix licenses:)
	     (guix git-download)

	     (gnu)
	     (gnu packages autotools)
	     (gnu packages tls)
	     (gnu services web)
	     (gnu packages ncurses)
	     (gnu packages certs)
	     (gnu packages tls)
	     (gnu packages virtualization)
	     (gnu packages linux)
	     (gnu packages emacs)
	     (gnu packages screen)
	     (gnu packages dns)
	     (gnu packages vpn)
	     (gnu packages rsync)
	     (gnu packages version-control)
	     (gnu packages docker)

	     (gnu services web)
	     (gnu services certbot)
	     (gnu services networking)
	     (gnu services ssh)
	     (gnu services docker)
	     (gnu services dbus)
	     (gnu services desktop)
	     (gnu services shepherd)
	     (gnu services virtualization))

(define cams-desktop-rsa-key
   (plain-file "cams_desktop_rsa.pub" "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDNXg/AIxp7Qi46EBGeZ5mChdCyhKKMcsmNkOL6S2QR0oMcLUyv9OEZVe3YWUQpkt85E0TxktETnyzE/DzPDQogzNQvbXk6UJ7w5+sUKHC/fYFKaGU5gKP2eG6xv0/IL212lU20m3ElW5tEqvg3ZVyy1cgixJkA9cxLJsSyuPOC3+0eCM3hRY+XyRDFMcWQjABAe0S6MkreesFJXbJQxKPCVEYOamDvnavs9JYvemHBphmcd/W6BgEs9YTB9qQhtUd2fd/skvCvunF1zeyu5nlTZXqnaZFBQxtSlYZDYIB9gRmoiuSa1lHy6fHdONci9Qh8yM2nniT+57PE7GRkLw5RA5p12Y2VH4N2HcLcdlUFv3HrhjyNTab2wuvKqFBf0HLIMmuVjNWbgHRPQLi79TTxx1UrtYfR1nu0LHclvt15Tt+NruF61tUD+jNjp8A3+qAYA7z/+JQyoub16u5nYrmnmyyMTa1hOGrNcC7kIXH9vEi1VSbsjA81ssXIUh9IW8M= root@DESKTOP-GL6EKCV
"))

(define cam-at-tindall-dot-space-rsa-key
  (plain-file "cam-at-tindall-dot-space-rsa.pub" "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQC2wwrp0T1BSmaqYMutkdUIUjJy5dfyqf1vwxQuHNZLYN7tJXHcKcMCcN1eYVNF1F/HPgmAcixbPULpniHerxeejLGAB/4dRo4X7H8UJUwf0Hh9rsLMC9lrS3HedNkAMjeQo4/rzaNB0z16/THX1vJWHlu6vx4hCqiiZk94B87UWSeMmvETSZDhCWBv1Ga61HE/HclbzX16NwVC1k3HYIzvogRMVf+vb/Tk8//g+lpaC/UTHSbPDYQoYPMEsb70i11Spp2+P4PE275y5Gc1FtdM2jZIkXgFEQ1z/tWlwteYhXdhj/TQ3PSWAppG+JctCAmbg15b5BfCzzrPvagVGQ2n cam@tindall.space
"))

(operating-system
 (host-name "new.tindall.space")
 (timezone "UTC")
 (locale "en_US.utf8")

 (bootloader (bootloader-configuration
              (bootloader grub-bootloader)
              (target "/dev/vda")))
 
 (file-systems (append (list (file-system
			      (device "/dev/vda2")
			      (mount-point "/")
			      (type "ext4")))
                       %base-file-systems))
 
 (groups %base-groups)
 
 (users (append (list (user-account
		       (name "cam")
		       (group "users")
		       (supplementary-groups (list "wheel"))
		       (home-directory "/home/cam")))
		%base-user-accounts))

 (sudoers-file
  (plain-file "sudoers"
              (string-append (plain-file-content %sudoers-specification)
                             (format #f "~a ALL = NOPASSWD: ALL~%"
                                     "cam"))))
 
 (packages (append (list ncurses ;; for whatever reason, this is the package that provides "clear"
			 le-certs nss-certs
			 screen
			 net-tools (list isc-bind "utils") ;; netstat, dig, etc.
			 rsync
			 git) 
		   %base-packages))
 
 (services (append (list (service dhcp-client-service-type)
			 (service openssh-service-type
				  (openssh-configuration
				   (permit-root-login #t)
				   (password-authentication? #t)
                                   (port-number 22)
				   (authorized-keys
				    `(("root" ,cam-at-tindall-dot-space-rsa-key)
				      ("cam"  ,cams-desktop-rsa-key)
				      ("cam"  ,cam-at-tindall-dot-space-rsa-key)
				      ))))
                         (elogind-service))
                   %base-services)))
