(use-modules (guix)
	     (guix packages)
	     (guix build-system gnu)
	     ((guix licenses) #:prefix licenses:)
	     (guix git-download)
	     
	     (gnu)
	     (gnu packages autotools)
	     (gnu packages tls)
	     )
(package
 (name "nsd20464-pwsafe")
 (version "2020-09-13")
 (source
  (origin
   (method git-fetch)
   (uri (git-reference
	 (url "https://github.com/nsd20463/pwsafe.git")
	 (commit "c49a0541b66647ad04d19ddb351d264054c67759")))
   (sha256
    (base32
     "0ak09r1l7k57m6pdx468hhzvz0szmaq42vyr575fvsjc8rbrp8qq"))))

 (build-system gnu-build-system)

 (native-inputs `(("autoconf" ,autoconf-2.68)
                  ("automake" ,automake)))

 (inputs `(("openssl" ,openssl)))

 (arguments
  '(#:configure-flags '("--disable-depenency-tracking")
		      #:phases (modify-phases %standard-phases
					      (add-before 'configure 'do-autoconf
							  (lambda* (#:key outputs #:allow-other-keys)
								   (invoke "aclocal")
								   (invoke "autoheader")
								   (invoke "automake" "--add-missing")
								   (invoke "autoconf")))
					      (delete 'check)
					      (add-before 'do-autoconf 'debug-shit
							  (lambda* (#:key outpus #:allow-other-keys)
								   (invoke "ls"))))))
 
 (synopsis "commandline tool compatible with Counterpane's Passwordsafe")
 (description "Development can be found at https://github.com/nsd20463/pwsafe

Releases and screenshots used to be found at http://nsd.dyndns.org/pwsafe
but that domain is dead due to things beyond my control. So there's just
github now.")
 (home-page "https://github.com/nsd20463/pwsafe")
 (license licenses:gpl2+))
